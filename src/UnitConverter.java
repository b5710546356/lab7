/**
 * UnitConverter class is a converter unit to another unit.
 * @author Phasin Sarunpornkul
 *
 */
public class UnitConverter {

	/**
	 * convert unit to another unit.
	 * @param amount is a value that want to convert.
	 * @param fromUnit is a main unit that want to convert.
	 * @param toUnit is a unit that want to be.
	 * @return result value that be converted.
	 */
	public double convert(double amount,Unit fromUnit,Unit toUnit){
		return  fromUnit.convertTo(amount, toUnit);
	}
	
	/**
	 * to get the list of units.
	 * @return the list of units type array.
	 */
	public Unit[] getUnit(){
		return Length.values();
	}
}
