/**
 * Main class for show ConverterUI 
 * @author Phasin Sarunpornkul
 *
 */
public class Main {
	/**
	 * to run main for show ConverterUI
	 * @param args is argument for run String
	 */
	public static void main(String[] args) {
		UnitConverter uc = new UnitConverter(); 
		ConverterUI ui = new ConverterUI(uc);
		ui.setVisible(true);
	}
}
