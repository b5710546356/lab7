/**
 * Unit Interface is interface that contain convertTo,getValue and toString method.
 * @author Phasin Sarunpornkul
 *
 */
public interface Unit {
	/**
	 * convert this unit to another unit by have value.
	 * @param amt is value that want to convert.
	 * @param unit is a unit that want to be.
	 * @return  result value that be converted.
	 */
	public double convertTo(double amt,Unit unit);
	/**
	 * to get this value.
	 * @return the value of this Length.
	 */
	public double getValue();
	/**
	 * to print information of this Length.
	 * @return name of this Length.
	 */
	public String toString();
}
