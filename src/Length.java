/**
 * Enum that contain unit of Length.
 * @author Phasin Sarunpornkul
 *
 */
public enum Length implements Unit  {
	METER("meter",1.0),
	KILOMETER("kilometer",1000.0),
	CENTIMETER("centimeter",0.001),
	MILLIMETER("millimeter",0.0001),
	NANOMETER("nanometer",0.000000001),
	MILE("mile",1609.344),
	FOOT("foot",0.30480),
	WA("wa",2.0),
	INCH("inch",0.0254),
	PARSEC("parsec",30856775813060000.0),
	LIGHT_YEAR("light year",9460730472581000.0),
	FURLONG("furlong",201.168),
	YARD("yard",5.0292),
	FATHOM("fathom",1.8288);
	
	/*Name of this Length*/
	public final String name;
	/*the value of this Length*/
	public final double value;
	
	/**
	 * Constructor foe new Length
	 * @param name is Name of this Length
	 * @param value is the value of this Length
	 */
	private Length(String name, double value){
		this.name = name;
		this.value = value;
	}
	
	/**
	 * convert this unit to another unit by have value.
	 * @param amt is value that want to convert.
	 * @param unit is a unit that want to be.
	 * @return  result value that be converted.
	 */
	public double convertTo(double amt,Unit unit){
		return amt*this.getValue()/unit.getValue();
	}
	
	/**
	 * to get this value.
	 * @return the value of this Length.
	 */
	public double getValue(){
		return this.value;
	}
	
	/**
	 * to print information of this Length.
	 * @return name of this Length.
	 */
	public String toString(){
		return this.name;
	}
	
}
