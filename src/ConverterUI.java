import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Converter Program with User Graphic Interface
 * @author Phasin Sarunpornkul
 *
 */
public class ConverterUI extends JFrame
{
	// attributes for graphical components
	private JButton convertButton,clearButton;
	private JLabel label1;
	private JTextField inputField1,inputField2;
	private JComboBox combobox1,combobox2;
	private UnitConverter unitconverter;

	/**
	 * Constructor for new ConverterUI
	 * @param UnitConverter type
	 */
	public ConverterUI( UnitConverter uc ) {
		this.unitconverter = uc;
		this.setTitle("Length Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents( );
	}

	/**
	 * initialize components in the window
	 */
	private void initComponents() {
		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );
		convertButton = new JButton("Convert");
		inputField1 = new JTextField(10);
		combobox1 = new JComboBox(Length.values());
		label1 = new JLabel("=");
		combobox2 = new JComboBox(Length.values());
		inputField2 = new JTextField(10);
		inputField2.setEditable(false);
		clearButton = new JButton("Clear");
		contents.add( inputField1 );
		contents.add( combobox1 );
		contents.add(label1);
		contents.add( inputField2 );
		contents.add( combobox2 );
		contents.add( convertButton );
		contents.add( clearButton );
		ActionListener listener = new ConvertButtonListener( );
		inputField1.addActionListener(listener);
		convertButton.addActionListener( listener );
		ActionListener listener2 = new ClearButtonListener( );
		clearButton.addActionListener( listener2 );
		this.pack(); // resize the Frame to match size of components
	}

	class ConvertButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			String s = inputField1.getText().trim();
			try{
				double value = Double.valueOf( s );
				double result = unitconverter.convert(value,(Unit)combobox1.getSelectedItem(),(Unit)combobox2.getSelectedItem());
				inputField2.setText(String.format("%.4g", result));
			}
			catch(NumberFormatException e){
				JOptionPane.showMessageDialog(null, "The input must be a number","ERROR",JOptionPane.INFORMATION_MESSAGE);
			}
		}
	} // end of the inner class for ConvertButtonListener

	class ClearButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			inputField1.setText("");
			inputField2.setText("");
		}
	} // end of the inner class for  ClearButtonListener
}